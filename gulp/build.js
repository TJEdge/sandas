"use strict";

var gulp   = require('gulp'),
    path   = require('path'),
    runSeq = require('run-sequence'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*', 'del']});

// ---------------------------------------------;
// Gulp build tasks                             ;
// ---------------------------------------------;
// Using gulp 3.9, so run-sequence ensures
// our build steps are executed synchronously
// to ensure prior build output is removed
// before compiliation;

// Build development version;
function buildFn(cb) {
	runSeq(
		'clean-dev'
		,[
			'dev'
			,'compile-dev'
		]
		,cb
	);
}

gulp.task('build-dev', buildFn);

// Build distribution (production) version;
gulp.task(
	'build-dist',
	function(callback) {
		runSeq(
		[
			'dist',
			'compile-dist'
		],
		callback);
});

gulp.task(
	'dist',
	['clean-dist'],
	function () {
		config.isProduction = true;
});

gulp.task(
	'dev',
	function () {
		config.isProduction = false;
});

// build with sourcemaps (no minify)
gulp.task(
	'sourcemaps',
	['usesources', 'default']);

gulp.task(
	'usesources',
	function () {
		config.useSourceMaps = true;
	});