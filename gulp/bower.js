"use strict";

var gulp   = require('gulp'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*', 'main-bower-files']});

// Install files for project from bower;
gulp.task(
	'bower-install',
	function() {
		return $.bower();
	});


// Move components installed in bower to the output foler;
gulp.task(
	'bower-files',
	function() {
		return gulp
			.src(
				$.mainBowerFiles(),
				{base:config.paths.source.bower})
			.pipe(gulp.dest(config.paths.output.dev.lib));
	});