/*
	******************************************************************************
	FILE NAME:   main.js
	------------------------------------------------------------------------------
	DESCRIPTION: Support interactive functionality and animation;
	------------------------------------------------------------------------------
	AUTHOR:      TJE
	CREATED:     2017-02-21
	******************************************************************************
*/
(function (jllsite, $, undefined) {
	"use strict";

	/*******************************************************/
	/* PRIVATE VARIABLES;																	*/
	/*******************************************************/
	// Domain element cache;
	function Selector_Cache()
		{
		// Collection of requested DOM objects;
	  var collection = {};

	  function get_from_cache(selector)
	  	{
	  	// Lazy initialize DOM object cache on request;
	    if (undefined === collection[selector])
	    	{
	      collection[selector] = $(selector);
	      }
			return collection[selector];
	  	}

	  // Expose public accessor method;
	  return {get: get_from_cache};
		}

	// Application DOM cache store;
	const $c = new Selector_Cache();


	/*******************************************************/
	/* PRIVATE METHODS;                                    */
	/*******************************************************/
	function initUserInter() {

		// Show nav submenu on hover;
		// var hoverSubMenu = function() {
		// 	let $subm = $c.get('ul.nav li.dropdown'), $tst;

		// 	$subm.on(
		// 		'hover',
		// 		function() {
		// 			$tst = $(this).find('.dropdown-menu').fadeIn(350);
		// 			console.log($tst);
		// 		},
		// 		function() {
		// 			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(350);
		// 		}
		// 	);
		// }
	// // On dropdown open
	// $(document).on(
	// 	'#mainSearch shown.bs.dropdown',
	// 	function(event) {
	// 		var dropdown = $(event.target);

	// 		// Set aria-expanded to true
	// 		dropdown.find('.dropdown-menu').attr('aria-expanded', true);

	// 		// Set focus on the first link in the dropdown
	// 		setTimeout(
	// 			function() {
	// 				dropdown.find('.dropdown-menu li:first-child a').focus();
	// 			}, 10);
	// 		});
	// // On dropdown close
	// $(document).on(
	// 	'#mainSearch  hidden.bs.dropdown',
	// 	function(event) {
	// 		var dropdown = $(event.target);

	// 		// Set aria-expanded to false
	// 		dropdown.find('.dropdown-menu').attr('aria-expanded', false);

	// 		// Set focus back to dropdown toggle
	// 		dropdown.find('.dropdown-toggle').focus();
	// });


  $('#mainMenu .dropdown').hover(
  	function() {
  		let $mnu = $(this).find('.dropdown-menu').first().stop(true,true);
    	$mnu.slideDown('350ms','easeOutQuart');
  	},
  	function() {
  		let $mnu = $(this).find('.dropdown-menu').first().stop(true,true);
    	$mnu.hide('0ms');
  	});

	}

	/*******************************************************/
	/* PUBLIC METHODS;                                     */
	/*******************************************************/
	jllsite.init = function() {
		// Initialize the interface actions and content;
		initUserInter();    // Initialize user interface actions;
	};

}(window.jllsite = window.jllsite || {}, jQuery));

/* Initialize the interface; */
jllsite.init();


/*
 * This is based on ideas from a technique described by Alen Grakalic in
 * http://cssglobe.com/post/8802/custom-styling-of-the-select-elements
 */
(function($) {
  $.fn.customSelect = function(settings) {
    var config = {
      replacedClass: 'replaced', // Class name added to replaced selects
      customSelectClass: 'custom-select', // Class name of the (outer) inserted span element
      activeClass: 'active', // Class name assigned to the fake select when the real select is in hover/focus state
      wrapperElement: '<div class="custom-select-container" />' // Element that wraps the select to enable positioning
    };
    if (settings) {
      $.extend(config, settings);
    }
    this.each(function() {
      var select = $(this);
      select.addClass(config.replacedClass);
      select.wrap(config.wrapperElement);
      var update = function() {
        val = $('option:selected', this).text();
        span.find('span span').text(val);
      };
      // Update the fake select when the real select’s value changes
      select.change(update);
      /* Gecko browsers don't trigger onchange until the select closes, so
       * changes made by using the arrow keys aren't reflected in the fake select.
       * See https://bugzilla.mozilla.org/show_bug.cgi?id=126379.
       * IE normally triggers onchange when you use the arrow keys to change the selected
       * option of a closed select menu. Unfortunately jQuery doesn’t seem able to bind to this.
       * As a workaround the text is also updated when any key is pressed and then released
       * in all browsers, not just in Firefox.
       */
      select.keyup(update);
      /* Create and insert the spans that will be styled as the fake select
       * To prevent (modern) screen readers from announcing the fake select in addition to the real one,
       * aria-hidden is used to hide it.
       */
      // Three nested spans? The only way I could get text-overflow:ellipsis to work in IE7.
      var span = $('<span class="' + config.customSelectClass + '" aria-hidden="true"><span><span>' + $('option:selected', this).text() + '</span></span></span>');
      select.after(span);
      // Change class names to enable styling of hover/focus states
      select.bind({
        mouseenter: function() {
          span.addClass(config.activeClass);
        },
        mouseleave: function() {
          span.removeClass(config.activeClass);
        },
        focus: function() {
          span.addClass(config.activeClass);
        },
        blur: function() {
          span.removeClass(config.activeClass);
        }
      });
    });
  };
})(jQuery);

$(function() {
  $('select.custom').customSelect();
});