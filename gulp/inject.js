/**
 * Tasks for plugging file dependencies into the environment
 */

'use strict';

var gulp = require('gulp'),
    path = require('path'),
    config = require('./config/config'),
    $ = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'main-bower-files', 'wiredep']
    });

var wiredep = $.wiredep.stream;

/*
* Primary task for injecting into source files
*/
gulp.task('inject');


// Injects all references into jade files
gulp.task('inject-jade',
	[
	'scripts-app'
	], function () {


    return gulp.src(config.paths.source.jade + "/**/" + config.globs.jade)
        .pipe($.inject(bowerSources, {
            name:'bower',
            ignorePath:'bower_components',
            addRootSlash:false,
            addPrefix:'lib'
        }))
        .pipe(gulp.dest(config.paths.output.dev.templates));
});
