/**
 * Copy image files to proper output folder;
 */
var gulp   = require('gulp'),
    path   = require('path'),
    runSeq = require('run-sequence'),
    merge  = require('merge-stream'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*']});

// Move media folder contents to output;
gulp.task('media-copy',
	function() {
		// Copy general media;
		var med = gulp.src(config.paths.source.assets.media + '/**/*')
									.pipe($.plumber(
										function (error) {
											$.util.log('Media copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dev.media));

		// Copy icon folder;
		var ico = gulp.src(config.paths.source.assets.icons + '/**/*')
									.pipe($.plumber(
										function (error) {
											$.util.log('Icons copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dev.icons));

		// Copy favicon to root;
		var fav = gulp.src(config.paths.source.assets.icons + '/favicon.ico')
									.pipe($.plumber(
										function (error) {
											$.util.log('Favicon copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dist));

		// Copy browserconfig to root;
		var brw = gulp.src(config.paths.source.assets.icons + '/browserconfig.xml')
									.pipe($.plumber(
										function (error) {
											$.util.log('BrowserConfig copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dist));

		// Copy manifest to root;
		var man = gulp.src(config.paths.source.assets.icons + '/manifest.json')
									.pipe($.plumber(
										function (error) {
											$.util.log('Manifest copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dist));

		// Copy fonts;
		var fon = gulp.src(config.paths.source.assets.fonts + '/**/*')
									.pipe($.plumber(
										function (error) {
											$.util.log('Fonts copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dev.fonts));

		// Copy objects;
		var obj = gulp.src(config.paths.source.assets.objects + '/**/*')
									.pipe($.plumber(
										function (error) {
											$.util.log('Objects copy ' + error.message);
											this.emit('end');
										}
									))
									.pipe(gulp.dest(config.paths.output.dev.objects));

		// Return will emit files in order added;
		return merge(med, ico, fav, brw, man, fon, obj);
});

// Handle asset processing;
gulp.task('assets-app',
	function(callback) {
		runSeq(
		'media-copy',
		function (err) {
		//if any error happened in the previous tasks, exit with a code > 0
			if (err) {
				var exitCode = 2;
				console.log('[ERROR] gulp build task failed', err);
				console.log('[FAIL] gulp build task failed - exiting with code ' + exitCode);
				return process.exit(exitCode);
			}
			else {
				return callback();
			}
		});
	});