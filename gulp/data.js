/**
 * Manage demo data;
 */
var gulp   = require('gulp'),
    path   = require('path'),
    runSeq = require('run-sequence'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*']});

// Copy local data files to proper output folder;
gulp.task('data-copy',
	function () {
		var datinp = config.paths.source.data + '/**/*',
				datout = config.paths.output.dev.data;

		return gulp.src(['./src/data/**/*'])
								.pipe($.plumber(
									function (error) {
										$.util.log('Data copy ' + error.message);
										this.emit('end');
									}
								))
		           .pipe(gulp.dest('./dev/data'));
	});

// gulp.task('data-copy',
// 	function() {
// 		// Copy demo data;
// 		console.log('src: ' + config.paths.source.data + ' out: ' + config.paths.output.dev.data);
// 		return gulp.src(config.paths.source.data + '/**/*')
									// .pipe($.plumber(
									// 	function (error) {
									// 		$.util.log('Data copy ' + error.message);
									// 		this.emit('end');
									// 	}
									// ))
// 									.pipe(gulp.dest(config.paths.output.dev.data));
// });

// Manage data;
gulp.task('data-app',
	function(callback) {
		runSeq(
		'data-copy',
		function (err) {
		//if any error happened in the previous tasks, exit with a code > 0
			if (err) {
				var exitCode = 2;
				console.log('[ERROR] gulp build task failed', err);
				console.log('[FAIL] gulp build task failed - exiting with code ' + exitCode);
				return process.exit(exitCode);
			}
			else {
				return callback();
			}
		});
	});