/**
 * Compilation of Compass files;
 */
var gulp   = require('gulp'),
    path   = require('path'),
    runSeq = require('run-sequence'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*']});

// ---------------------------------------------;
// CSS sources                                  ;
// ---------------------------------------------;
//var bootSass = config.paths.source.bower + '/' + config.paths.source.bootstrap.scss;
// Copy normalize.min.css to dev (grab *.map as well);
var normalizeCss = config.paths.source.assets.css + '/**/*.*';

// SASS .scss file options;
// Development version;
var scssDev = {
	inp:   path.join(config.root, config.paths.source.compass.main),
	out:   config.paths.output.dev.styles,
	opts:  {
		outputStyle: 'expanded'    // Expanded output for debugging;
		,precision:  3             // Float precision using recommended value;
		,sourceComments: true      // Verbose comments;
		,includePaths:             // Include paths to external libraries;
			[
			//bootSass                 // Bootstrap;
			]
	}
};

// OPTIONAL: If using Font Awesome;
var scssFontAwesomeDev = {
	inp:   path.join(config.root, config.paths.source.compass.main, config.paths.source.compass.fontAwesome),
	out:   config.paths.output.dev.styles,
	opts:  {
		outputStyle: 'expanded'    // Expanded output for debugging;
		,precision:  3             // Float precision using recommended value;
		,sourceComments: true      // Verbose comments;
		,includePaths:             // Include paths to external libraries;
			[
			//bootSass                 // Bootstrap;
			]
	}
};

// Distribution version;
var scssDist = {
	inp:   path.join(config.root, config.paths.source.compass.main),
	out:   path.join(config.root, config.paths.output.dist),
	opts:  {
		outputStyle: 'compressed'  // Expanded output for debugging;
		,precision:  3             // Float precision using recommended value;
		,sourceComments: false     // Verbose comments;
		,includePaths:             // Include paths to external libraries;
			[
			//bootSass                 // Bootstrap;
			]
	}
};

// ---------------------------------------------;
// Gulp build tasks                             ;
// ---------------------------------------------;
// Compile Application CSS;
gulp.task(
	'styles-app',
	[
		'copy-reset',
		'app-compass'
	]);

// Copy normalize.css reset to output folder;
gulp.task(
	'copy-reset',
	function () {
		return gulp.src(normalizeCss)
			.pipe($.plumber(
				function (error) {
					$.util.log(error.message);
					this.emit('end');
				}
			))
			.pipe(gulp.dest(config.paths.output.dev.styles));
	}
);

// Copy required font files to output folder;
gulp.task('app-font',
	function() {
		var fontinppath = config.paths.source.compass.fonts + '/**/*',
				fontoutpath = config.paths.output.dev.fonts;

		// Copy font files to output folder;
		return gulp.src(fontinppath)
			.pipe($.plumber(
				function (error) {
					$.util.log(error.message);
					this.emit('end');
				}
			))
			.pipe(gulp.dest(fontoutpath,{overwrite:false}));
	}
);

gulp.task('app-fontawesome',
	function() {
		var fontinppath = config.paths.source.bower + '/' + config.paths.source.fontawesome + '/**/*',
				fontoutpath = config.paths.output.dev.fonts + '/FontAwesome';

		// Copy font files to output folder;
		return gulp.src(fontinppath)
			.pipe($.plumber(
				function (error) {
					$.util.log(error.message);
					this.emit('end');
				}
			))
			.pipe(gulp.dest(fontoutpath,{overwrite:false}));
	}
);

// Build application CSS;
gulp.task(
	'app-compass',
	function () {
		return gulp
			.src(scssDev.inp + "/*" + config.globs.scss)
			.pipe($.plumber(
				function (error) {
					$.util.log(error.message);
					this.emit('end');
				}
			))
			.pipe($.compass({
				css:  scssDev.out,
				sass: config.paths.source.compass.main
				}))
			.pipe(gulp.dest(scssDev.out))
	});

// Build FontAwesome CSS;
gulp.task(
	'app-compass-fa',
	function () {
		return gulp
			.src(scssFontAwesomeDev.inp + "/*" + config.globs.scss)
			.pipe($.plumber(
				function (error) {
					$.util.log(error.message);
					this.emit('end');
				}
			))
			.pipe($.compass({
				css:  scssDev.out,
				sass: config.paths.source.compass.main
				}))
			.pipe(gulp.dest(scssDev.out))
	});

// Error handler
function handleError(err) {
	console.log(err.toString());
	this.emit('end');
}