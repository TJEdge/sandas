"use strict";

var gulp   = require('gulp'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*']});

// Jade Templates;
gulp.task('templates', function () {
	// Only return index;
	return gulp.src('src/views/' + config.globs.jade)
		.pipe($.plumber(
			function (error) {
				$.util.log(error.message);
				this.emit('end');
			}
		))
		.pipe($.jade({pretty:true}))
		.pipe(gulp.dest('./dev',{overwrite:false}));
	// var outpath = config.paths.output.dev.base;

	// gulp.src(config.paths.source.jade + "/**/" + config.globs.jade)
	// 	.pipe($.plumber(
	// 		function (error) {
	// 			$.util.log(error.message);
	// 			this.emit('end');
	// 		}
	// 	))
	// 	.pipe($.jade({
	// 	  pretty: true
	// 	}))
	// 	.pipe(gulp.dest('./dev',{overwrite:false}));
});

// Error handler
function handleError(err) {
    console.log(err.toString());
    this.emit('end');
}