"use strict";

var gulp   = require('gulp'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*']});

// Process javascript files; right now, only copy folders;
gulp.task(
	'scripts-app',
	function () {
		return gulp.src(config.paths.source.scripts + "/**/" + config.globs.javascript)
			.pipe($.plumber(
				function (error) {
					$.util.log(error.message);
					this.emit('end');
				}
			))
			//.pipe($.angularFilesort())
			//.pipe(config.useSourceMaps ? $.sourcemaps.init() : $.util.noop())
			//.pipe($.ngAnnotate())
			//.pipe(config.isProduction ? $.uglify({preserveComments: 'some'}) : $.util.noop())
			//.pipe(config.useSourceMaps ? $.sourcemaps.write() : $.util.noop())
			.pipe(gulp.dest(config.paths.output.dev.scripts));
	}
);