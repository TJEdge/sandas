"use strict";

var gulp   = require('gulp'),
    path   = require('path'),
    fs     = require('fs-extra'),
    del    = require('del'),
    runSeq = require('run-sequence'),
    wait   = require('gulp-wait'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*', 'del']});

// Delete development output and folder;
// **********************************************
// This awful synchronous mess needed
// to make this run without ENOENT errors
// on Windows 8.1.
// **********************************************
gulp.task('clean-js-mod', function (cb) {
  return del('./dev/js/modules/**/*.*');
});

gulp.task('clean-js-ven', function (cb) {
  return del('./dev/js/vendor/**/*.*');
});

gulp.task('clean-css', function (cb) {
  return del('./dev/css/**/*.*');
});

gulp.task('clean-js', function (cb) {
  return del('./dev/js/**');
});

gulp.task('clean-img', function (cb) {
  return del('./dev/media/images/**/*.*');
});

gulp.task('clean-fonts', function (cb) {
  return del('./dev/fonts/**/*.*');
});

gulp.task('clean-folders', function(cb) {
	return del('./dev');
});

function cleanDevBuild(cb) {
// Uncomment or comment these to stop errors;
	return runSeq(
	  'clean-js-mod'
	, 'clean-js-ven'
	, 'clean-js'
	, 'clean-css'
	, 'clean-img'
	, 'clean-fonts'
	, 'clean-folders'
	, cb
	);
}

gulp.task('clean-dev', cleanDevBuild);

// Delete distribution output and folder;
gulp.task('clean-dist', function (cb) {
  return fs.remove(config.paths.output.dist, cb);
});