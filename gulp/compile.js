"use strict";

var gulp   = require('gulp'),
    path   = require('path'),
    runSeq = require('run-sequence').use(gulp),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*', 'del']});

var semanticTemp = config.paths.output.dev.semantic + "/**";

//var assets = $.useref.assets();
gulp.task(
	'compile-all',
	function(callback) {
		runSeq(
		'clean-dev',
		'compile-assets'
		,'templates'
		,function (err) {
		//if any error happened in the previous tasks, exit with a code > 0
			if (err) {
				var exitCode = 2;
				console.log('[ERROR] gulp build task failed', err);
				console.log('[FAIL] gulp build task failed - exiting with code ' + exitCode);
				return process.exit(exitCode);
			}
			else {
				return callback();
			}
		});
	}
);

gulp.task('compile-templates', ['templates']);
gulp.task(
	'compile-assets',
	function(callback){
		runSeq(
 //   'bower-files'
    'data-copy',
    'scripts-app',
    'styles-app',
    'assets-app',
//    ,'compile-typescript'
    function (err) {
      //if any error happened in the previous tasks, exit with a code > 0
      if (err) {
        var exitCode = 2;
        console.log('[ERROR] gulp build task failed', err);
        console.log('[FAIL] gulp build task failed - exiting with code ' + exitCode);
        return process.exit(exitCode);
      }
      else {
        return callback();
      }
    });
	}
   //,'styles-themes'
);

gulp.task(
	'compile-dev',
	function(callback) {
		runSeq(
		'compile-all'
		,callback);

    //var outpath = path.join(config.root,config.paths.output.dev.base);

    //gulp.src(config.root + "/**/" + config.globs.html)
        //.pipe($.useref())
        //.pipe($.revReplace())
        //.pipe(gulp.dest('./dev'));
});

gulp.task('compile-dist', ['dist', 'compile-all', 'templates'],
	function () {
    var outpath = path.join(config.root,  config.paths.output.dist)

    gulp.src(config.paths.output.dev.base + "/**/" + config.globs.html)
        .pipe($.if('*.js', $.uglify()))
        .pipe($.rev())
        .pipe($.useref())
        .pipe($.revReplace())
        .pipe(gulp.dest(path.join(config.root, config.paths.output.dist)));
});