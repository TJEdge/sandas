/*
  ******************************************************************************
  FILE NAME:   _fonts-utils.scss
  ------------------------------------------------------------------------------
  DESCRIPTION: Define font mixins and variables for fluid-sizing and appearance;
  ------------------------------------------------------------------------------
  AUTHOR:      TJE
  CREATED:     2016-11-21
  ******************************************************************************
*/
// --------------------------------------------------------
// FONT STACKS & SIZES;
// --------------------------------------------------------
$font-sans: Arial,'Helvetica Neue','Helvetica Medium',Helvetica,sans-serif;
$font-open: 'Open Sans','Open Sans FB',Arial,'Helvetica Neue',Helvetica,sans-serif;
$font-robo: 'Roboto',Arial,'Helvetica Neue','Helvetica Medium',Helvetica,'Arial Neue',sans-serif;

// Font sizes;
$fs-pic: 0.44em;  // Pico;
$fs-nan: 0.50em;  // Nano;
$fs-mic: 0.60em;  // Micro;
$fs-tny: 0.66em;  // Tiny;
$fs-sml: 0.76em;  // Small;
$fs-med: 0.80em;  // Medium;
$fs-mep: 0.86em;  // Medium Plus;
$fs-fin: 0.90em;  // Regular Adjusted;
$fs-reg: 0.92em;  // Regular;
$fs-one: 1.00em;  // One Em;
$fs-onn: 1.10em;  // One Plus;
$fs-lrg: 1.20em;  // Large;
$fs-fam: 1.50em;  // Family;
$fs-ins: 1.60em;  // Institutional;
$fs-eco: 1.80em;  // Economy;
$fs-jum: 2.20em;  // Jumbo;
$fs-gia: 2.80em;  // Giant;

// Condensed spacing;
$condensed: -0.0125rem;

// --------------------------------------------------------
// FONT DEFINITION;
// --------------------------------------------------------
// Google webfont unicode range specifications (extend as needed);
$latin  : U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
$latin-ext    : U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
$cyrillic     : U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
$cyrillic-ext :  U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
$greek  : U+0370-03FF;
$greek-ext    : U+1F00-1FFF;

// Define local font face;
@mixin font-face($font-family, $file-path, $font-weight, $font-style, $range: $latin) {
  @font-face {
    font-family: $font-family;
      src: url('#{$file-path}.eot');
      src: url('#{$file-path}.eot?#iefix') format('embedded-opentype'),
           url('#{$file-path}.woff') format('woff'),
           url('#{$file-path}.ttf') format('truetype'),
           url('#{$file-path}.svg##{$font-family}') format('svg');
    font-weight: $font-weight;
    font-style: $font-style;
    unicode-range: $range;
  }
}

// --------------------------------------------------------
// FLUID FONTS;
// --------------------------------------------------------
// Minimum and maximum viewport sizes to apply the font scaling;
$min_width: 768;
$max_width: 960;

// These values represent the range of font-size to apply; values affect base
// font-size, headings and other elements will scale proportionally
$min_font: 8;
$max_font: 20;

:root { font-size: #{$min_font}px; }

// Set root font-size based upon viewport dimensions; use rem for font-sizing;
// Refer to: https://madebymike.com.au/writing/precise-control-responsive-typography/
@media (min-width: #{$min_width}px) and (max-width: #{$max_width}px) {
  :root {
    font-size: calc( #{$min_font}px + (#{$max_font} - #{$min_font}) * ( (100vw - #{$min_width}px) / ( #{$max_width} - #{$min_width}) ));
  }
}
@media (min-width: #{$max_width}px) {
   :root {
     font-size: #{$max_font}px;
   }
}


// Viewport sized typography with minimum and maximum values;
// @author Eduardo Boucas (@eduardoboucas)
//
// @param {Number}   $responsive  - Viewport-based size
// @param {Number}   $min         - Minimum font size (px)
// @param {Number}   $max         - Maximum font size (px)
//                                  (optional)
// @param {Number}   $fallback    - Fallback for viewport-
//                                  based units (optional)
//
// @example scss - 5vw font size (with 50px fallback),
//                 minumum of 35px and maximum of 150px
//  @include responsive-font(5vw, 35px, 150px, 50px);
//
@mixin responsive-font($responsive, $min, $max: false, $fallback: false) {
  $responsive-unitless: $responsive / ($responsive - $responsive + 1);
  $dimension: if(unit($responsive) == 'vh', 'height', 'width');
  $min-breakpoint: $min / $responsive-unitless * 100;

  @media (max-#{$dimension}: #{$min-breakpoint}) {
    font-size: $min;
  }

  @if $max {
    $max-breakpoint: $max / $responsive-unitless * 100;

    @media (min-#{$dimension}: #{$max-breakpoint}) {
      font-size: $max;
    }
  }

  @if $fallback {
    font-size: $fallback;
  }

  font-size: $responsive;
}

// --------------------------------------------------------
// TEXT DISPLAY;
// --------------------------------------------------------
// Prevent text selection;
@mixin prevent-text-select {
  -ms-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}

// Attempt better font-smoothing on Chrome and Mozilla;
@mixin font-smoothing($value: antialiased) {
  @if $value == antialiased {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  @else {
    -webkit-font-smoothing: subpixel-antialiased;
    -moz-osx-font-smoothing: auto;
  }
}

// Attempt to improve legibility and display quality;
// Currently expects a hex value for font color;
@mixin enhance-text-display($fontHexColor) {
	font-kerning:normal;
	@include font-feature-settings(kern liga);
	@include font-smoothing(antialiased);
	text-shadow:1px 1px 1px rgba($fontHexColor,0.004);
	text-rendering:optimizeLegibility;
}

// Add font-feature-settings support;
// from: https://github.com/bitmanic/font-feature-settings;
@mixin font-feature-settings($feature-list: false) {
	// Only proceed if a list of arguments was passed
	@if $feature-list
	{
		// Create an empty list for the standard syntax
		$standard: ();

		// Create an empty list for the old Mozilla syntax
		$old-moz: ();

		// Add each listed feature to the standard and old Mozilla values
		@each $feature in $feature-list
		{
			$standard: append( $standard, '#{$feature}' 1, comma );
			$old-moz: append( $old-moz, '#{$feature}=1', comma );
		}

		// Finally, print out the prefixed and non-prefixed code for all of the listed features
		-moz-font-feature-settings: $old-moz;
		-moz-font-feature-settings: $standard;
		-ms-font-feature-settings: $standard;
		-o-font-feature-settings: $standard;
		-webkit-font-feature-settings: $standard;
		font-feature-settings: $standard;
	}
}