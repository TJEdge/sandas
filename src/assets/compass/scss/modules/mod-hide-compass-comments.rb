# ******************************************************************************
# FILE NAME:   mod-hide-multiline-comments.rb
# ------------------------------------------------------------------------------
# DESCRIPTION: Hide multi-line comments from output;
#              To use, require file from Compass config.rb;
# ------------------------------------------------------------------------------
# AUTHOR:      TJE
# CREATED:     2016-09-21
# ******************************************************************************
class Sass::Tree::Visitors::Perform < Sass::Tree::Visitors::Base

  # Removes all comments completely
  def visit_comment(node)
    return []
  end

end