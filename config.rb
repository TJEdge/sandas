# Require any additional compass plugins here.
require 'breakpoint'
require 'animation'
require 'bootstrap-sass'

# Remove multiline comments using a monkey patch;
require "./src/assets/compass/scss/modules/mod-hide-compass-comments"

# Define project paths and directories;
http_path       = '/'                         # For Compass plugins;
relative_assets = false                       # Not at root;
css_dir         = 'dev/css'                   # Output folder for CSS;
sass_dir        = 'src/assets/compass/scss'   # Location of working files;
images_dir      = 'src/assets/media/images'   # Images base folder;
fonts_url       = 'src/assets/fonts'          # Fonts base folder;
javascripts_dir = 'src/scripts'               # Working scripts folder;
cache_path      = 'tmp/.sass_cache'           # Cached files;

# Expanded or compressed CSS formatting;
output_style = :expanded
# output_style = :compressed

# Include comments;
line_comments = true
#line_comments = false

# Using Compass over SASS;
preferred_syntax = :scss

# Use sourcemaps;
sourcemap = true
#sourcemap = false
