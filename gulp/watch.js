"use strict";

var gulp   = require('gulp'),
    path   = require('path'),
    config = require('./config/config'),
    $      = require('gulp-load-plugins')({pattern: ['gulp-*', 'browser-sync']});

var browserSync = $.browserSync.get(config.browserSyncName);

//----------------------------------------------;
// WATCH                                        ;
//----------------------------------------------;
function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString())

  this.emit('end')
}
// Run tasks when a file changes;
gulp.task(
	'watch',
	['compile-all'],
	function () {
		// Base;
		gulp.watch(config.paths.source.base + "/**/" + config.globs.javascript, ['scripts-app']);
		gulp.watch(config.paths.source.base + "/**/" + config.globs.scss,       ['styles-app']);
		gulp.watch(config.paths.source.base + "/**/" + config.globs.jade,       ['templates']);

		// Data;
		// gulp.watch(config.paths.source.base + "/**/" + config.globs.csv,        ['data-app']);
		// gulp.watch(config.paths.source.base + "/**/" + config.globs.tsv,        ['data-app']);

		// Images;
		// Cannot start paths here with ./ or / and have files copy properly;
		gulp.watch("src/**/" + config.globs.png, ['assets-app']);
		gulp.watch("src/**/" + config.globs.svg, ['assets-app']);
		gulp.watch("src/**/" + config.globs.gif, ['assets-app']);

		//Watch dev folder for reload;
		gulp.watch([config.paths.output.dev.base + '/**']).on('change', browserSync.reload);
});

gulp.task(
	'compass',
	function () {
		return gulp.src([config.paths.source.compass.main + "/*" + config.globs.scss])
			.pipe(compass({
				config_file: './config.rb',
				css: config.paths.output.dev.styles,
				sass: config.paths.source.compass.main,
				task: 'watch'
			}))
	});

gulp.task(
	'default',
	[
		'browser-sync',
		'watch',
		'compass'
	]);